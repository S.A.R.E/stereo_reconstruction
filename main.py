from cProfile import label
from shutil import ExecError
import cv2
import numpy as np
import glob
import sys
import time

iter_num = 42
labels_num = 16
window_radius = 2

border = labels_num
window_size = (2 * window_radius + 1) ** 2

directions = [[-1, 0],  # left
              [1, 0],  # right
              [0, -1],  # up
              [0, 1]]  # down


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result
    return timed



class Node(object):
    def __init__(self):
        """
        Initialize the potentials of the node to zeros and the messages to all zeros
        """
        self.potential = np.zeros(labels_num)
        self.msg = np.zeros((4, labels_num))
        self.map_assignment = 0


def belief(n, label):
    """
    Given a node n and a label, return the belief of that node

    :param n: the node you want to update
    :param label: the label we want to compute the belief of
    :return: The belief of the node.
    """
    result = n.potential[label]
    # for i in range(4):
    result += sum(n.msg[:,label])
    return result


def pairwiseCost(i, j):
    """
    Given two integers, i and j, return 16 times the minimum of the absolute difference between i and j
    and 4

    :param i: The index of the first node in the pair
    :param j: The index of the current node
    :return: The cost of the edge between i and j.
    """
    return 16 * min(abs(i - j), 4)


@timeit
def calcMAP(graph):
    """
    Find the label with the lowest cost for each node and assign it to that node

    :param graph: the graph to perform the calculation on
    :return: The graph with the MAP assignment for each node.
    """
    for n in graph:
        min_cost = belief(n, 0)
        n.map_assignment = 0

        for i in range(1, labels_num):
            cost = belief(n, i)

            if (cost < min_cost):
                min_cost = cost
                n.map_assignment = i
    return graph


class Markov(object):
    def __init__(self, right_img_path, left_img_path):
        """
        Read the images and initialize the messages

        :param right_img_path: the path to the right image
        :param left_img_path: the path to the left image
        """
        self.width = 0
        self.height = 0
        self.size = 0
        self.img_r = 0
        self.img_l = 0
        self.graph = 0
        self.readImgs(right_img_path, left_img_path)
        self.initMsgs()

    @timeit
    def run(self):
        """
        The function iterates through the number of iterations specified by the user. 
        For each iteration, it prints the iteration number. 
        Then, it updates the messages in the graph. 
        Finally, it calculates the MAP of the graph and writes the results to a file
        """
        for i in range(iter_num):
            print("Iter #" + str(i))
            self.updateMsgs()
        self.graph = calcMAP(self.graph)
        self.writeResults()

    @timeit
    def readImgs(self, right_img_path, left_img_path):
        """
        Read the right and left images and store them in the variables img_r and img_l

        :param right_img_path: Path to the right image
        :param left_img_path: The path to the left image
        """
        self.img_r = cv2.imread(right_img_path, 0)
        self.img_l = cv2.imread(left_img_path, 0)
        self.width = self.img_r.shape[1]
        self.height = self.img_r.shape[0]
        self.size = self.width * self.height

    def unaryCost(self, x, y, disparity):
        """
        The function calculates the average absolute difference between the pixels in the window around the
        pixel at (x,y) in the left image and the corresponding pixel in the right image

        :param x: the x coordinate of the pixel in the left image
        :param y: the y coordinate of the pixel in the left image
        :param disparity: the disparity value at (x, y)
        :return: The average difference between the pixels in the window.
        """
        sum_diff = 0
        for yy in range(y - window_radius, y + window_radius + 1):
            for xx in range(x - window_radius, x + window_radius + 1):
                pix_l = self.img_l[yy, xx]
                pix_r = self.img_r[yy, xx - disparity]
                sum_diff += abs(pix_l - pix_r)
        avg_diff = sum_diff / window_size
        return avg_diff

    @timeit
    def initMsgs(self):
        def setValue(y, x):
            """
            The function is used to set the potential of each node in the graph.
                The potential of each node is a numpy array of shape (labels_num,).
                The potential of each node is set to be the unary cost of the node

            :param y: The y-coordinate of the pixel
            :param x: The x coordinate of the pixel
            """
            self.graph[y * self.width + x].potential[:] = np.array(list(map(
                self.unaryCost, [x]*labels_num, [y]*labels_num, [k for k in range(labels_num)])))

        def setValue_x(y):
            x_list = list(range(border, self.width - border))
            list(map(setValue, [y]*len(x_list), x_list))

        self.graph = [Node() for i in range(self.size)]
        y_list = list(range(border, self.height - border))
        list(map(setValue_x, y_list))
        # for y in range(border, self.height - border):
        ## x_list = list(range(border, self.width - border))
        ## list(map(setValue, [y]*len(x_list), x_list))
        # for x in range(border, self.width - border):
        #     # setValue(y, x)
        #     for k in range(labels_num):
        #         self.graph[y * self.width + x].potential[k] = self.unaryCost(x, y, k)

    def validCoordinates(self, x, y):
        """
        Check if the given coordinates are valid

        :param x: The x coordinate of the cell
        :param y: The y-coordinate of the top left corner of the rectangle
        :return: True if the coordinates are valid and False otherwise.
        """
        return (x >= 0) and (x < self.width) and (y >= 0) and (y < self.height)

    def updateMsg(self, x, y, direction, label):
        """
        For each node, update the message to each of its neighbors by subtracting the message from the
        previous iteration and adding the new message

        :param x: The x coordinate of the sender node
        :param y: The y coordinate of the sender node
        :param direction: the direction of the message
        :param label: the label of the message
        :return: The updated message from the sender node to the receiver node.
        """
        nx = x + directions[direction][0]
        ny = y + directions[direction][1]
        opposite_direction = direction ^ 1
        if self.validCoordinates(nx, ny):
            sender = self.graph[y * self.width + x]
            min_cost = sys.maxsize
            labels_list = [k for k in range(labels_num)]
            pairwiseCost_cost = np.array(
                list(map(pairwiseCost, [label]*labels_num, labels_list)))
            belief_cost = np.array(
                list(map(belief, [sender]*labels_num, labels_list)))
            sender_cost = np.array(sender.msg[direction,:])
            min_cost = np.min(pairwiseCost_cost + belief_cost - sender_cost)
            self.graph[ny * self.width + nx].msg[opposite_direction,label] = min_cost
            
    def updateMsg_d(self, x, y, label):
        sender = self.graph[y * self.width + x]
        min_cost = sys.maxsize
        labels_list = [k for k in range(labels_num)]
        pairwiseCost_cost = np.array(
            list(map(pairwiseCost, [label]*labels_num, labels_list)))
        belief_cost = np.array(
            list(map(belief, [sender]*labels_num, labels_list)))
        for direction in range(4): 
            nx = x + directions[direction][0]
            ny = y + directions[direction][1]
            opposite_direction = direction ^ 1
            if self.validCoordinates(nx, ny):
                sender_cost = np.array(sender.msg[direction,:])
                min_cost = np.min(pairwiseCost_cost + belief_cost - sender_cost)
                self.graph[ny * self.width + nx].msg[opposite_direction,label] = min_cost
    @timeit
    def updateMsgs(self):
        """
        For each pixel in the image,
        for each direction, for each label, update the message
        """
        def setValue(x, y, d):
            list(map(self.updateMsg, [x]*labels_num, 
                     [y]*labels_num, [d]*labels_num, 
                     [k for k in range(labels_num)]))
        def setValue_x(y, d):
            x_list = list(range(0, self.width))
            list(map(setValue, x_list, [y]*len(x_list), [d]*len(x_list)))
            
            
        def setValue_d(x, y):
            list(map(self.updateMsg_d, [x]*labels_num, 
                     [y]*labels_num, [k for k in range(labels_num)]))
        def setValue_x_d(y):
            x_list = list(range(0, self.width))
            list(map(setValue_d, x_list, [y]*len(x_list)))
            
        # y_list = list(range(0, self.height))
        # list(map(setValue_x_d, y_list))
        for d in range(4):
            y_list = list(range(0, self.height))
            list(map(setValue_x, y_list, [d]*len(y_list)))
        #     for y in range(0, self.height):
        #     ## x_list = list(range(0, self.width))
        #     ## list(map(setValue, x_list, [y]*len(x_list), [d]*len(x_list)))
        #         for x in range(0, self.width):
        #             setValue(x, y, d)
        #             # for i in range(labels_num):
        #                 # self.updateMsg(x, y, d, i)

    @timeit
    def writeResults(self):
        """
        For each pixel in the image, we assign it to the cluster with the nearest centroid
        """
        reconstruction = np.zeros((self.height, self.width), dtype=np.uint8)
        for y in range(0, self.height):
            for x in range(0, self.width):
                reconstruction[y, x] = self.graph[y * self.width + x].map_assignment * (256 / labels_num)
        cv2.imwrite("data/img_out.png", reconstruction)


# The `if __name__ == "__main__":` is a special construct in Python.
#     If the module is run as a script, it will execute the contained code.
#     If the module is imported, it will not run the contained code.
#     In this case, the code is run when we run the script, so the Markov class is initialized and the
# run function is called.
#     If the Markov class is imported, the code in the `if __name__ == "__main__":` block is not run.
if __name__ == "__main__":
    try:
        right_img_path = sys.argv[1]
    except Exception as e:
        right_img_path = "data/right.png"
    try:
        left_img_path = sys.argv[2]
    except Exception as e:
        left_img_path = "data/left.png"

    m = Markov(right_img_path, left_img_path)
    m.run()
    # img_r, img_l, width, height, size = readImgs(right_img_path, left_img_path)
    # graph = initMsgs(img_r, img_l, width, height, size)

    # for i in range(iter_num):
    #     print("Iter #" + str(i))
    #     graph = updateMsgs(graph)

    # calcMAP(graph)
    # writeResults(graph)
