<span style="font-family: IRANSansX">


<div dir="rtl">

<p style="font-size:24pt; font-style:italic">
گزارش روش بازسازی تصاویر استریو با کمک روش Markov Random Field
</p>
<p style="font-size:18pt; font-style:italic">
سید علیرضا اصفهانی
</p>

[شرح مسئله](#شرح-مسئله)

[مدل MRF](#مدل-MRF)

[روش های انتشار باور](#روش-های-انتشار-باور)

[نتایج](#نتایج)

## شرح مسئله

در این مسائل یک جفت تصویر استریو داریم(مانند تصاویر پایین) و به دنبال راه های بازسازی اطلاعات عمق از این تصاویر هستیم.
در این مسائل دو تصویر کمی از نظر محل دوربین و زاویه ی دوربین می توانند متفاوت باشند. مانند تصاویری که به کمک چشم های چپ و راست مشاهده می کنیم.



<table border="1">
<tbody>
<tr>
<td>
<p></p><figure id="attachment_1367" aria-describedby="caption-attachment-1367" style="width: 270px" class="wp-caption aligncenter"><img class=" wp-image-1367 " title="imL" src="https://i.postimg.cc/ht7NPfD1/left.png" alt="" width="270" height="203" srcset="https://i.postimg.cc/ht7NPfD1/left.png 300w, https://i.postimg.cc/ht7NPfD1/left.png 384w" sizes="(max-width: 270px) 100vw, 270px"></a><figcaption id="caption-attachment-1367" class="wp-caption-text">Left image</figcaption></figure></td>
<td>
<p></p><figure id="attachment_1368" aria-describedby="caption-attachment-1368" style="width: 270px" class="wp-caption aligncenter"><img loading="lazy" class=" wp-image-1368 " title="imR" src="https://i.postimg.cc/Kvjdw0CV/right.png" alt="" width="270" height="203" srcset="https://i.postimg.cc/Kvjdw0CV/right.png 300w, https://i.postimg.cc/Kvjdw0CV/right.png 384w" sizes="(max-width: 270px) 100vw, 270px"></a><figcaption id="caption-attachment-1368" class="wp-caption-text">Right image</figcaption></figure></td>
</tr>
</tbody>
</table>



وقتی ما در حال حرکت هستیم انتظار داریم اجسامی که به ما نزدیکتر هستند با سرعت بیشتری نسبت به اجسام دورتر حرکت کنند. از همین ایده برای حل این مسئله استفاده می کنیم یعنی به عنوان مثال اختلاف پیکسل ها در روی چراغ مطالعه نسبت به مجسمه کمتر باشد.

<img src="https://i.postimg.cc/WbyQvMv4/stereo.png" alt="drawing" style="width:800px;"/>

## مدل MRF

Markov Random Field مدل های گرافی بدون جهت هستند که می توانند وابستگی های خاص را شناسایی کنند و کد گذاری کنند. از آنجا که این مدل ها بدون جهت هستند می توانند دارای دور باشند.


<img src="https://i.postimg.cc/N02ntw2V/Screenshot-from-2022-02-20-12-41-26.png" alt="drawing" style="width:800px;"/> 


در تصویر بالا گره های آبی مشاهدات می از پیسکل ها می باشند و گره های سبز ویژگی های پنهانی که با توجه به ارتباطات بین همسایه ها و گره به وجود آمده را در خود جای داده اند، در این الگوریتم ما تلاش می کنیم تا ارتباطات بین گره ها را تشخیص دهیم. در ادامه به این متغییر های label می گوییم.

هر پیکسل به چهار پیکسل اطراف خود اتصال دارد. پنابراین (مانند تصویر بالا) هر گره پنهان به چهار گره پنهان اطراف خود(همسایه ها) و پیکسلی که به آن مربوط است، ارتباط دارد.

بنابراین تابع انرژی عبارت است از



لینک‌های بین هر گره نشان‌دهنده یک وابستگی است. بنابراین برای مثال، گره پنهان پیکسل تنها به چهار گره پنهان همسایه خود و گره مشاهده‌شده بستگی دارد. این فرض نسبتا قوی است که حالت یک نود تنها به neighours آنی آن بستگی دارد که یک فرض مارکوف نامیده می‌شود. زیبایی این فرض ساده این است که به ما اجازه می‌دهد تا متغیرهای پنهان را به شیوه‌ای معقول حل کنیم.

<img src="https://i.postimg.cc/kGcG3Hnw/Screenshot-from-2022-02-20-13-49-04.png" alt="drawing" style="width:800px;"/>

متغیر Y گره مشاهده شده و متغیر X گره پنهان متصل به Y است، i نشان دهنده ی ایندکس پیکسل و j نیز همسایه ی آن است x(i) نیز اشاره دارد به تعداد لایه های گره پنهان برای هر متغیر مشاهده شده(Y)، در این تابع x(i) ها همان label ها هستند.

هدف یافتن label برای مشهاده ی Y است که کمترین هزینه را تولید کند.

تابع DataCost یا همان تابع پتانسیل هزینه ی اختصاص یک label را به گره Y(i) محاسبه میکند. این هزینه برابر است با اختلاف مقدار پیکسل Y با پیکسلی در فاصله ی label قرار دارد.

</div>

``` python
def DataCost(i, label):
    y = int(i / imageWidth) # integer round down 
    x = i - y * imageWidth

    d = abs(leftImage(x,y) - rightImage(x - label,y))

    return d
```
<div dir="rtl">

تابع SmoothnessCost یا همان pairwise potential مقایسه ای بین گره x(i) و گره های مجاور انجام میدهد. 
نمونه هایی از این توابع در عکس زیر موجود است.

<img src="https://i.postimg.cc/d1qNqWrD/Screenshot-from-2022-02-20-14-35-56.png" alt="drawing" style="width:800px;"/>

## روش های انتشار باور

 تصویر مورد بحث که در بخش [شرح مسئله](#شرح-مسئله) نشان داده شده است ابعادی برابر با 384 و 288 پیکسل دارد که در کل برابر است با 384×288 = 110592 . اگر برای هر پیکسل 16 همسایه ی مجاور را بررسی کنیم، کل محاسبات برابر است با 16^110592.

 بنابراین ما نمی توانیم تمام حالات ممکن را بررسی کنیم.

 در الگوریتم انتشار باور هر گره زمانی که اطلاعاتش کامل شد، اطلاعات خود را به گره های مجاور خود ارسال میکند، و زمانی اطلاعات یک گره کامل می گردد که از تمام همسایه های خود اطلاعات را دریافت کرده باشد.

. نمونه ی انتشار باور در عکس زیر قابل مشاهده است.

<img src="https://justhalf.github.io/pgm-spring-2019/assets/img/notes/lecture-12/pgm345.png" alt="drawing" style="width:800px;"/>

شبه کد الگوریتم را به صورت زیر می توان بازنویسی نمود

</div>

```python

def LoopyBeliefPropgation():

    initialise all messages

    for t in iterations:
        at every pixel pass messages right
        at every pixel pass messages left
        at every pixel pass messages up
        at every pixel pass messages down
    
    find the best label at every pixel i by calculating belief
end

```

<div dir="rtl">

چون هر گره باید ابتدا از تمام گره های همسایه خود تمام اطلاعات را دریافت کند تا سپس بتواند اطلاعات خود را انتشار بدهد، بنابراین تمام گره ها منتظر همدیگر می مانند و الگوریتم اجرا نمی شود. برای رفع این مشکل تمام گره ها را با یک مقدار ثابت initialise می کنیم.

این الگوریتم سه بخش اصلی دارد

### message update



<!-- <img src="" alt="drawing" style="width:800px;"/> -->
<!-- <img src="" alt="drawing" style="width:800px;"/> -->
<!-- <img src="" alt="drawing" style="width:800px;"/> -->
<!-- <img src="" alt="drawing" style="width:800px;"/> -->
<!-- <img src="" alt="drawing" style="width:800px;"/> -->
<!-- <img src="" alt="drawing" style="width:800px;"/> -->



<!-- <img src="https://i.postimg.cc/Kvjdw0CV/right.png" alt="drawing" style="width:800px;"/>  -->







<div dir="rtl">

</div>